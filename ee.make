; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; ------------

projects[drupal][type] = core

; Collegesites profile
; --------------------

projects[collegesites][download][type] = "git"
projects[collegesites][download][tag] = "7.x-13.0.2"
projects[collegesites][download][url] = "https://bitbucket.org/wwuweb/collegesites.git"
projects[collegesites][type] = "module"

; Modules
; ------------------

projects[better_exposed_filters][version] = 3.6
projects[better_exposed_filters][type] = "module"

projects[block_class][version] = 2.4
projects[block_class][type] = "module"

projects[ds][version] = 2.16
projects[ds][type] = "module"

projects[field_formatter_settings][version] = 1.1
projects[field_formatter_settings][type] = "module"

projects[linked_field][version] = 1.10
projects[linked_field][type] = "module"

projects[panel_variant_page_title][version] = 1.5
projects[panel_variant_page_title][type] = "module"

projects[paragraphs][version] = 1.0-rc5
projects[paragraphs[type] = "module"

projects[paragraphs_pack][version] = 1.0-alpha5
projects[paragraphs_pack][type] = "module"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"

projects[video_filter][version] = 3.4
projects[video_filter][type] = "module"

projects[views_extra_pagers][version] = 1.0
projects[views_extra_pagers][type] = "module"

projects[views_load_more][version] = 1.5
projects[views_load_more][type] = "module"

projects[views_xml_backend][version] = 1.0-alpha4
projects[views_xml_backend][type] = "module"

projects[void_menu][version] = 1.9
projects[void_menu][type] = "module"

projects[webform_mailchimp][version] = 2.0-beta1
projects[webform_mailchimp][type] = "module"

projects[webform_panels][version] = 1.1
projects[webform_panels][type] = "module"

projects[workbench_og][version] = 2.0-beta1
projects[workbench_og][type] = "module"
projects[workbench_og][patch][] = "https://www.drupal.org/files/issues/workbench_og-node_access-2835937.patch"

; Custom
; ------

projects[ee_degree_mods][download][type] = "git"
projects[ee_degree_mods][download][url] = "https://bitbucket.org/wwuweb/ee_degree_mods.git"
projects[ee_degree_mods][destination] = "modules"
projects[ee_degree_mods][type] = "module"

projects[ee_og_menu_alter][download][type] = "git"
projects[ee_og_menu_alter][download][url] = "https://bitbucket.org/wwuweb/ee_og_menu_alter.git"
projects[ee_og_menu_alter][destination] = "modules"
projects[ee_og_menu_alter][type] = "module"

; Themes
; --------

projects[shiny][version] = 1.7
projects[shiny][type] = "theme"

projects[wwuzen_dual_ee][download][type] = "git"
projects[wwuzen_dual_ee][download][url] = "https://bitbucket.org/wwuweb/wwuzen-dual-ee.git"
projects[wwuzen_dual_ee][type] = "theme"

; Features
; ----------

libraries[ee_features][download][type] = "git"
libraries[ee_features][download][url] = "https://bitbucket.org/wwuweb/ee_features.git"
libraries[ee_features][destination] = "modules"

libraries[paragraphs_accordion][download][type] = "git"
libraries[paragraphs_accordion][download][url] = "https://bitbucket.org/wwuweb/paragraphs_accordion.git"
libraries[paragraphs_accordion][destination] = "modules"
